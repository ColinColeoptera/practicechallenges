using WebAppChallenges.Logic.NumberGuessingGame;

namespace TestProject
{
    public class NumberGuessingGameTests
    {
        [Fact]
        public void AssignEasyDifficultyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(1);
            //Assert
            Assert.Equal(1, game.Difficulty);
        }
        [Fact]
        public void AssignMediumDifficultyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(2);
            //Assert
            Assert.Equal(2, game.Difficulty);
        }
        [Fact]
        public void AssignHardDifficultyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(3);
            //Assert
            Assert.Equal(3, game.Difficulty);
        }
        [Fact]
        public void AssignInvalidDifficultyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(4);
            //Assert
            Assert.Equal(0, game.Difficulty);
        }


        [Fact]
        public void RandomNumberGeneratorEasyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(1);
            game.GetTargetNumber();
            //Assert
            Assert.Equal(5, game.Target);
        }
        [Fact]
        public void RandomNumberGeneratorMediumTest()
        {
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(2);
            game.GetTargetNumber();
            //Assert
            Assert.Equal(55, game.Target);
        }
        [Fact]
        public void RandomNumberGeneratorHardTest()
        {
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(3);
            game.GetTargetNumber();
            //Assert
            Assert.Equal(555, game.Target);
        }
        [Fact]
        public void RandomNumberGeneratorInvalidTest()
        {
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(10);
            game.GetTargetNumber();
            //Assert
            Assert.Equal(0, game.Target);
        }
        [Fact]
        public void GuessCorrectEasyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(1);
            game.GetTargetNumber();
            //Assert
            Assert.Equal("Correct!", game.Guess(5));
        }
        [Fact]
        public void GuessTooHighEasyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(1);
            game.GetTargetNumber();
            //Assert
            Assert.Equal("Too high!", game.Guess(6));
        }
        [Fact]
        public void GuessTooLowEasyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(1);
            game.GetTargetNumber();
            //Assert
            Assert.Equal("Too low!", game.Guess(2));
        }
        [Fact]
        public void GuessSameTooLowEasyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(1);
            game.GetTargetNumber();
            game.Guess(2);
            //Assert
            Assert.Equal("You already guessed that! Too low!", game.Guess(2));

        }
        [Fact]
        public void GuessCorrectMediumTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(2);
            game.GetTargetNumber();
            //Assert
            Assert.Equal("Correct!", game.Guess(55));
        }
        [Fact]
        public void GuessTooHighMediumTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(2);
            game.GetTargetNumber();
            //Assert
            Assert.Equal("Too high!", game.Guess(60));
        }
        [Fact]
        public void GuessTooLowMediumTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(2);
            game.GetTargetNumber();
            //Assert
            Assert.Equal("Too low!", game.Guess(20));
        }
        [Fact]
        public void GuessSameTooLowMediumTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(2);
            game.GetTargetNumber();
            game.Guess(20);
            //Assert
            Assert.Equal("You already guessed that! Too low!", game.Guess(20));

        }
        [Fact]
        public void GuessCorrectHardTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(3);
            game.GetTargetNumber();
            //Assert
            Assert.Equal("Correct!", game.Guess(555));
        }
        [Fact]
        public void GuessTooHighHardTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(3);
            game.GetTargetNumber();
            //Assert
            Assert.Equal("Too high!", game.Guess(600));
        }
        [Fact]
        public void GuessTooLowHardTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(3);
            game.GetTargetNumber();
            //Assert
            Assert.Equal("Too low!", game.Guess(200));
        }
        [Fact]
        public void GuessSameTooLowHardTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame(new TestRandomGenerator());
            //Act
            game.SetDifficulty(3);
            game.GetTargetNumber();
            game.Guess(200);
            //Assert
            Assert.Equal("You already guessed that! Too low!", game.Guess(200));
        }
    }
}