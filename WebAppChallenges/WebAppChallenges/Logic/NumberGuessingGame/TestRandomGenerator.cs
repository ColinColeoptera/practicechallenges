﻿namespace WebAppChallenges.Logic.NumberGuessingGame
{
    public class TestRandomGenerator : IRandomGenerator
    {
        public int GenerateRandom(int difficulty)
        {
            switch (difficulty)
            {
                case 1:
                    return 5;
                case 2:
                    return 55;
                case 3:
                    return 555;
                default:
                    return 0;
            }
        }
    }
}
