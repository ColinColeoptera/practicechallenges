﻿namespace WebAppChallenges.Logic.NumberGuessingGame
{
    public class RandomGenerator : IRandomGenerator
    {
        public  int GenerateRandom(int difficulty)
        {
            int max = 0;
            Random random = new Random();
            switch (difficulty)
            {
                case 1:
                    max = 11;
                    break;
                case 2:
                    max = 101;
                    break;
                case 3:
                    max = 1001;
                    break;
                default:
                    max = 1;
                    break;
            }
            int randomNumber = random.Next(1, difficulty);
            return randomNumber;
        }
    }
}
