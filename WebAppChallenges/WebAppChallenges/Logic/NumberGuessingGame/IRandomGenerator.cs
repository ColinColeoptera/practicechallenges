﻿namespace WebAppChallenges.Logic.NumberGuessingGame
{
    public interface IRandomGenerator
    {
        public int GenerateRandom(int difficulty);
    }
}
