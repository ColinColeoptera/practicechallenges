﻿namespace WebAppChallenges.Logic.NumberGuessingGame
{
    public class NumberGuessingGame
    {
        private IRandomGenerator _randomGenerator;
        private List<int> Guesses = new List<int>();
        public NumberGuessingGame(IRandomGenerator randomGenerator)
        {
            _randomGenerator = randomGenerator;
        }
        public NumberGuessingGame()
        {
            _randomGenerator = new RandomGenerator();
        }
        public int Difficulty { get; set; }
        public int Target { get; set; }

        public void Run()
        {  
        }

        public void SetDifficulty(int difficulty)
        {
            if (difficulty < 1 || difficulty > 3)
            {
                Difficulty = 0;
            }
            else
            {
                Difficulty = difficulty;
            }
        }

        public void GetTargetNumber()
        {
            Target = _randomGenerator.GenerateRandom(Difficulty);
        }
        public string Guess(int guess)
        {
            string result = "";
            if (Guesses.Contains(guess)) result += "You already guessed that! ";
            if (guess == Target)
            {
                result += "Correct!";
            }
            else if (guess > Target)
            {
                result += "Too high!";
            }
            else
            {
                result += "Too low!";
            }
            Guesses.Add(guess);
            return result;
        }
    }
}
