﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAppChallenges.Controllers
{
    public class NumberGuessingGameController : Controller
    {
        // GET: NumberGuessingGameController
        public ActionResult Index()
        {
            return View();
        }

        // GET: NumberGuessingGameController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
         
        // GET: NumberGuessingGameController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NumberGuessingGameController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: NumberGuessingGameController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: NumberGuessingGameController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: NumberGuessingGameController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: NumberGuessingGameController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
