using ConsoleChallenges.HumanNumberGuessingGame;
namespace Tests
{
    public class NumberGuessingTests
    {
        //Difficulty Input
        [Fact]
        public void EasyDifficultySelectTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(1);
            //Assert
            Assert.Equal(1, game.Difficulty);
        }
        [Fact]
        public void MediumDifficultySelectTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(2);
            //Assert
            Assert.Equal(2, game.Difficulty);
        }
        [Fact]
        public void HardDifficultySelectTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(3);
            //Assert
            Assert.Equal(3, game.Difficulty);
        }
        [Fact]
        public void InvalidDifficultySelectTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(4);
            //Assert
            Assert.Equal(0, game.Difficulty);
        }

        [Fact]
        public void EasyNumberChosenTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(1);
            //Assert
            Assert.True(game.Target <= 10 && game.Target >= 1);
        }
        [Fact]
        public void MediumNumberChosenTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(2);
            //Assert
            Assert.True(game.Target <= 100 && game.Target >= 1);
        }
        [Fact]
        public void HardNumberChosenTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(3);
            //Assert
            Assert.True(game.Target <= 1000 && game.Target >= 1);
        }
        [Fact]
        public void InvalidNumberChosenTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(4);
            //Assert
            Assert.Equal(0, game.Target);
        }

        [Fact]
        public void LowGuessEasyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(1);
            //Assert
            Assert.Equal("Too Low. ", game.Guess(1));
        }
        [Fact]
        public void HighGuessEasyTest()
        {
            //Arrange
            NumberGuessingGame game = new NumberGuessingGame();
            //Act
            game.SetDifficulty(1);
            //Assert
            Assert.Equal("Too High. ", game.Guess(10));
        }
        [Fact]
        public void CorrectGuessEasyTest()
        {
        }
    }
}