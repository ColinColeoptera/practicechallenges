﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenges.KarvonenHeartRate
{
    public class KarvonenHeartCalculator
    {
        public int Age { get; set; }
        public int RestingHeartRate { get; set; }
        public int TargetHeartRate { get; set; }
        public void Run()
        {
            Console.Clear();
            WriteColor("Let's calculate your target heart rate!", ConsoleColor.Red);
            bool calculating = true;
            while (calculating)
            {
                calculating = Calculate();
            }
            Console.WriteLine("GoodBye!", ConsoleColor.Blue);
        }
        private bool Calculate()
        {
            WriteColor("How old are you? ", ConsoleColor.Blue);
            Age = GetInput();
            WriteColor("What's your resting heart rate? ", ConsoleColor.Blue);
            RestingHeartRate = GetInput();
            //WriteColor("What's your target heart rate? ", ConsoleColor.Blue);
            //TargetHeartRate = GetInput();
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine();
            WriteColor($"Resting Pulse: ", ConsoleColor.Gray);
            WriteColorLine($"{RestingHeartRate} \t ", ConsoleColor.DarkRed);
            WriteColor("Age: ", ConsoleColor.Gray);
            WriteColorLine($"{Age}", ConsoleColor.Red);
            Console.WriteLine();
            WriteColorLine("Intensity\t| Rate", ConsoleColor.Black);
            WriteColorLine("------------|--------", ConsoleColor.Black);
            for (int i = 55; i <= 95; i += 5)
            {
                WriteColor($"{i}%\t\t| ", ConsoleColor.Black);
                WriteColorLine($"{Math.Round((((220 - Age) - RestingHeartRate) * (i / 100.0)) + RestingHeartRate)} bpm", ConsoleColor.DarkRed);
            }
            Console.BackgroundColor = ConsoleColor.Black;
            WriteColor("Would you like to calculate another target heart rate? (y/n): ", ConsoleColor.Blue);
            string input = Console.ReadLine();
            if (input == "y" || input == "Y") return true;
            else return false;
        }
        private int GetInput()
        {
            int input;
            while (!int.TryParse(Console.ReadLine(), out input))
            {
                WriteColor("Please enter a valid number: ", ConsoleColor.DarkRed);
            }
            return input;
        }
        public void WriteColor(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(message);
            Console.ForegroundColor = ConsoleColor.Green;
        }
        public void WriteColorLine(string message, ConsoleColor color)
        {
            WriteColor(message, color);
            Console.WriteLine();
        }
    }
}
