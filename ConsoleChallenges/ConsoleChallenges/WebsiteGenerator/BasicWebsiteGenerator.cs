﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenges.WebsiteGenerator
{
    public class BasicWebsiteGenerator
    {
        // Website Generator
        //        Programming languages can create files and folders too.
        //        Create a program that generates a website skeleton with the
        //        following specifications:
        //• Prompt for the name of the site.
        //        • Prompt for the author of the site.
        //        • Ask if the user wants a folder for JavaScript files.
        //• Ask if the user wants a folder for CSS files.
        //• Generate an index.html file that contains the name of the
        //        site inside the<title> tag and the author in a<meta> tag.
        //Example Output
        //        Site name: awesomeco
        //        Author: Max Power
        //        Do you want a folder for JavaScript? y
        //        Do you want a folder for CSS? y
        //        Created./awesomeco
        //        Created ./awesomeco/index.html
        //        Created ./awesomeco/js/
        //Created./awesomeco/css/

        public string SiteName { get; set; }
        public string Author { get; set; }
        public bool JavaScript { get; set; }
        public bool CSS { get; set; }
        public void Run()
        {
            Console.Clear();
            WriteColor("Okk, you want to scaffold a basic a website.", ConsoleColor.Red);
            bool creating = true;
            while (creating)
            {
                creating = Create();
            }
            Console.WriteLine("GoodBye!", ConsoleColor.Blue);
        }
        private bool Create()
        {
            WriteColor("What's your site's name? ", ConsoleColor.Blue);
            SiteName = Console.ReadLine();
            WriteColor("Who's the site's author? ", ConsoleColor.Blue);
            Author = Console.ReadLine();
            WriteColor("Do you want a folder for your JavaScript? (y/n): ", ConsoleColor.Blue);
            JavaScript = ((Console.ReadLine() == "y") || (Console.ReadLine() == "Y"));
            WriteColor("Do you want a folder for your CSS? (y/n): ", ConsoleColor.Blue);
            CSS = ((Console.ReadLine() == "y") || (Console.ReadLine() == "Y"));
            Console.WriteLine();
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            WriteColorLine($"Created ./{SiteName}", ConsoleColor.Gray);
            WriteColorLine($"Created ./{SiteName}/index.html", ConsoleColor.Gray);
            if (JavaScript)
            {
                WriteColorLine($"Created ./{SiteName}/js/", ConsoleColor.Gray);
            }
            if (CSS)
            {
                WriteColorLine($"Created ./{SiteName}/css/", ConsoleColor.Gray);
            }
            Console.BackgroundColor = ConsoleColor.Black;
            WriteColor("Would you like to create another website? (y/n): ", ConsoleColor.Blue);
            string input = Console.ReadLine();
            if (input == "y" || input == "Y") return true;
            else return false;
        }
        private void WriteColor(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
        private void WriteColorLine(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
