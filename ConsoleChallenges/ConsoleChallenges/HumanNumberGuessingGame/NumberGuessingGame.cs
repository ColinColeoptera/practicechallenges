﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenges.HumanNumberGuessingGame
{
    public class NumberGuessingGame
    {
        public int Difficulty { get; set; }
        public int Target { get; set; }
        //TODO: Add a method allowing manual testing from cli
        public void Run()
        {
            Console.Clear();
            Console.WriteLine("Let's play Guess The Number!");
            bool playing = true;
            while (playing)
            {
                playing = Play();
            }
            Console.WriteLine("GoodBye!");
        }
        private bool Play()
        {
            List<int> guesses = new List<int>();

            while (Difficulty != 1 && Difficulty != 2 && Difficulty != 3)
            {
                Console.Write("Pick a difficulty level(");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("1");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(", ");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write("2");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(", or");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Write(" 3");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("): ");
                SetDifficulty(GetInput());
                if (Difficulty == 1 || Difficulty == 2 || Difficulty == 3) break;
            }
            Console.Write("I have my number. What's your guess?");
            bool guessing = true;
            do
            {
                int guess = GetInput();
                string message = Guess(guess);
                Console.Write(message);
                if (!guesses.Contains(guess)) guesses.Add(guess);
                if (guess == Target) guessing = false;
                if (message == "Correct!") Console.WriteLine();
                else Console.Write("Guess again: ");
            }
            while (guessing);
            Console.WriteLine($"You got it in {guesses.Count} guesses");
            Console.Write("Play again? (y/n): ");
            if (Console.ReadLine() == "n") return false;
            return true;
        }
        private int GetInput()
        {
            try
            {
                string rawInput = Console.ReadLine();
                int input;
                bool success = int.TryParse(rawInput, out input);
                if (success) return input;
                else Console.WriteLine("That's Not a number! Try Again.");
            }
            catch (Exception)
            {
                throw;
            }
            return 0;
        }
        public void SetDifficulty(int difficulty)
        {
            if (difficulty < 4 && difficulty > 0)
                Difficulty = difficulty;
            else Difficulty = 0;

            switch (Difficulty)
            {
                case 1:
                    Target = new Random().Next(1, 10);
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;
                case 2:
                    Target = new Random().Next(1, 100);
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    break;
                case 3:
                    Target = new Random().Next(1, 1000);
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    break;
                default:
                    Target = 0;
                    break;
            }
        }
        public string Guess(int guess)
        {
            if (guess == Target)
                return "Correct!";
            else if (guess > Target)
                return "Too High. ";
            else return "Too Low. ";
        }
    }
}
