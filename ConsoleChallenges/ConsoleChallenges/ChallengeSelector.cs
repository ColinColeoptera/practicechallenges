﻿using ConsoleChallenges.HumanNumberGuessingGame;
using ConsoleChallenges.KarvonenHeartRate;
using ConsoleChallenges.WebsiteGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenges
{
    public class ChallengeSelector
    {
        public void Run()
        {
            Console.Clear();
            Console.WriteLine("Welcome to the Console Challenge Selector!");
            bool selecting = true;
            while (selecting)
            {
                selecting = Select();
            }
            Console.WriteLine("GoodBye!");
        }
        private bool Select()
        {
            Console.WriteLine("Select a challenge to run:");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("1. Number Guessing Game");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(" (Completed 01/17/23)");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("2. Karvonen Heart Rate Calculator");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(" (Completed 01/23/23)");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("3. Basic Website Generator");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(" (Completed 01/29/23)");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Enter the number of the challenge you would like to run: ");
            int input = GetInput();
            switch (input)
            {
                case 1:
                    Console.Clear();
                    NumberGuessingGame game = new NumberGuessingGame();
                    game.Run();
                    return true;
                case 2:
                    Console.Clear();
                    KarvonenHeartCalculator calculator = new KarvonenHeartCalculator();
                    calculator.Run();
                    return true;
                case 3:
                    Console.Clear();
                    BasicWebsiteGenerator generator = new BasicWebsiteGenerator();
                    generator.Run();
                    return true;
                default:
                    Console.WriteLine("Invalid selection");
                    return true;
            }
        }
        private int GetInput()
        {
            int input;
            while (!int.TryParse(Console.ReadLine(), out input))
            {
                Console.Write("That wasn't right. Please enter an actual number: ");
            }
            return input;
        }
    }

}
